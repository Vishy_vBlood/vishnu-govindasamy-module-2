void main () {
   var a = new WinningAppInfo();
  a.name = "FNB Banking";
  a.sector_category =
      "best iOS consumer, best Blackberry and best Android consumer App";
  a.developer = "FirstRand Limited";
  a.yearitwon = 2012;

  var b = new WinningAppInfo();
  b.name = "Bookly";
  b.sector_category = "Most Innovative App App";
  b.developer = "SC TWODOOR GAMES SRL";
  b.yearitwon = 2013;

  var c = new WinningAppInfo();
  c.name = "Live-inspect";
  c.sector_category = "The overall winner";
  c.developer = "Radweb";
  c.yearitwon = 2014;

  var d = new WinningAppInfo();
  d.name = "CPUT-mobile";
  d.sector_category = "Best Enterprise Development App";
  d.developer = "The CPUT Web Office";
  d.yearitwon = 2015;

  var e = new WinningAppInfo();
  e.name = "Domestly";
  e.sector_category = "Best Consumer App";
  e.developer = "Berno Potgieter and Thatoyoana Marumo";
  e.yearitwon = 2016;

  var f = new WinningAppInfo();
  f.name = "Orderln";
  f.sector_category = "Consumer App";
  f.developer = "OrderIn HQ";
  f.yearitwon = 2017;

  var g = new WinningAppInfo();
  g.name = "Cowa-bunga";
  g.sector_category = "MTN Enterprise App of the Year Award";
  g.developer = "Cowabunga Team";
  g.yearitwon = 2018;

  var h = new WinningAppInfo();
  h.name = "Digger";
  h.sector_category = "Best Enterprise Solution Award";
  h.developer = "Wixels Digital (Pty) Ltd";
  h.yearitwon = 2019;

  var i = new WinningAppInfo();
  i.name = "Checkers Sixty60";
  i.sector_category = "Peoples Choice Award and Best Enterprise Solution";
  i.developer = "Shoprite Checkers (PTY) LTD";
  i.yearitwon = 2020;

  var j = new WinningAppInfo();
  j.name = "Amabani Africa";
  j.sector_category =
      "Best Gaming Solution, Best Educational and Best South African Solution";
  j.developer = "Amabani Africa";
  j.yearitwon = 2021;

  
  a.WinningAppDeails();
  b.WinningAppDeails();
  c.WinningAppDeails();
  d.WinningAppDeails();
  e.WinningAppDeails();
  f.WinningAppDeails();
  g.WinningAppDeails();
  h.WinningAppDeails();
  i.WinningAppDeails();
  j.WinningAppDeails();
}
class WinningAppInfo {
  String? name;
  String? sector_category;
  String? developer;
  int? yearitwon;

  void WinningAppDeails() {
    print("Answer for 3a");
    print(
        "The App Name $name.toUpperCase which won in the $sector_category category, by $developer and won in $yearitwon ");
    print("Answer for 3b");
    print('$name'.toUpperCase());
  }
}
